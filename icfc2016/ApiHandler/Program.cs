﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ApiHandler.ApiModels;
using Platform;
using Platform.Geometry;
using System.Diagnostics;
using NLog;
using NUnit.Framework;
using Platform.Models;
using Platform.SolverByFolding;
using System.Text.RegularExpressions;
using System.Threading;


namespace ApiHandler
{
    class Program
    {
        static private List<ProblemModel> GetProblems()
        {
            var problemsDir = @"d:\Projects\icfpc2016\icfc2016\FolderTool\Tasks\";
            var problemFiles = Directory.GetFiles(problemsDir, "*.json");

            var models = new List<ProblemModel>();

            foreach (var problemFile in problemFiles)
            {
                // problem_2519_05a6c69e1d574178c3a36822a764b78bcb07b898.json
                var problemText = File.ReadAllText(problemFile);

                var m = Regex.Match(problemFile, "problem_(\\d+)_");
                if (!m.Success)
                {                    
                    continue;
                }
                var id = Int32.Parse(m.Groups[1].Value);

                var model = ProblemParser.ParseProblem(problemText);
                model.ProblemId = id;
                models.Add(model);
            }
            return models;
        }          
        
        void SolveTask()
        {
            SolverFolder solver = new SolverFolder();
            ApiHandler h = new ApiHandler();

            var problems = GetProblems();

            foreach (var problemModel in problems.Where(p => p.ProblemId == 101))
            {
                try
                {
                    var result = solver.SolveProblem(problemModel);
                    var respond = h.SubmitSolution(problemModel.ProblemId, result.Solution);

                    if (respond.ok)
                    {
                        Console.WriteLine(
                            $"Problem {problemModel.ProblemId} solved! Resemplance:{respond.resemblance} Size:{respond.solution_size}");
                    }
                    else
                    {
                        Console.WriteLine($"Problem {problemModel.ProblemId} NOT solved. ok=false!");
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Problem {problemModel.ProblemId} NOT solved");
                }

            }
        }

        static void Main(string[] args)
        {
            var tools = new Tools();

            tools.SolveAll();
            //tools.WriteAllTasksToFiles();

        }
    }    
}
