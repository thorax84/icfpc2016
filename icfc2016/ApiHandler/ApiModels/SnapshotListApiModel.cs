﻿using System;

namespace ApiHandler.ApiModels
{
    public class SnapshotListApiModel
    {
        public String ok { get; set; }
        public SnapshotBriefApiModel[] snapshots { get; set; }
    }
}