﻿using System;

namespace ApiHandler.ApiModels
{
    public class SnapshotProblemApiModel
    {
        //public String ranking { get; set; }
        public String publish_time { get; set; }
        public Int32 solution_size { get; set; }
        public String problem_id { get; set; }
        public String owner { get; set; }
        public Int32 problem_size { get; set; }
        public String problem_spec_hash { get; set; }
    }
}