﻿using System;

namespace ApiHandler.ApiModels
{
    public class SnapshotBriefApiModel
    {
        public String snapshot_hash { get; set; }
        public String snapshot_time { get; set; }
    }
}