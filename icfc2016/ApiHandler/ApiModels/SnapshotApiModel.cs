﻿using System;

namespace ApiHandler.ApiModels
{
    public class SnapshotApiModel
    {
        public SnapshotProblemApiModel[] problems { get; set; }
        public String snapshot_time { get; set; }
        //public String leaderboard { get; set; }
        //public String users { get; set; }
    }
}