﻿namespace ApiHandler.ApiModels
{
    public class SolutionSubmitRespondApiModel
    {
        public bool ok;
        public int problem_id;
        public double resemblance;
        public string solution_spec_hash;
        public int solution_size;

        /// <summary>
        /// отражает версию солвера, который решал задачу
        /// </summary>
        public int Generation;
    }
}