﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using ApiHandler.ApiModels;
using Newtonsoft.Json;
using NLog;
using NLog.Fluent;
using Platform;
using Platform.Models;

namespace ApiHandler
{
    public class ApiHandler
    {
        private static readonly Logger Log = LogManager.GetLogger("API");

        static string ApiKey = "196-09f5ac7679413e4a764b055b4b4497c4";

        private static TimeSpan Second = new TimeSpan(0, 0, 1);
        DateTime lastSubmitSolution = DateTime.Now;
        public SolutionSubmitRespondApiModel SubmitSolution(Int32 problemId, String solution)
        {
            Log.Trace($"SubmitSolution for {problemId}");

            //% curl --compressed -L -H Expect: -H 'X-API-Key: 196-09f5ac7679413e4a764b055b4b4497c4' -F 'problem_id=1' -F 'solution_spec=@work/solution.txt' 'http://2016sv.icfpcontest.org/api/solution/submit'

            var uri = "http://2016sv.icfpcontest.org/api/solution/submit";

            var servicePoint = ServicePointManager.FindServicePoint(new Uri(uri));
            servicePoint.Expect100Continue = false;

            var c = GetClient();

            Dictionary<string, string> content = new Dictionary<string, string>
            {
                {"problem_id", problemId.ToString() },
                {"solution_spec", solution },
            };

            var body = FormatForm(content);

            //// запрет частых вызовов
            //var now = DateTime.Now;
            //var fromlastCall = now - lastSubmitSolution ; глючит
            //if (fromlastCall.Duration() <= Second)
            //{
            //    Thread.Sleep(Second);
            //}

            Thread.Sleep(Second);
            var result = c.UploadData(uri, body);
            lastSubmitSolution = DateTime.Now;

            var resultStr = Unzip(result);
            Log.Trace($"SubmitSolution for {problemId} respond: " + resultStr);
            var respond = JsonConvert.DeserializeObject<SolutionSubmitRespondApiModel>(resultStr);
            return respond;
        }

        public SolutionSubmitRespondApiModel SubmitProblem(DateTime time, String solution)
        {
            //% curl--compressed - L - H Expect: -H 'X-API-Key: 196-09f5ac7679413e4a764b055b4b4497c4' - F 'solution_spec=@work/solution.txt' - F 'publish_time=1470441600' 'http://2016sv.icfpcontest.org/api/problem/submit'
            var uri = "	http://2016sv.icfpcontest.org/api/problem/submit";

            var servicePoint = ServicePointManager.FindServicePoint(new Uri(uri));
            servicePoint.Expect100Continue = false;

            var c = GetClient();

            

            Dictionary<string, string> content = new Dictionary<string, string>
            {
                {"publish_time", GetTimestamp(time) },
                {"solution_spec", solution },
            };

            var body = FormatForm(content);

            var result = c.UploadData(uri, body);
            var resultStr = Unzip(result);

            var respond = JsonConvert.DeserializeObject<SolutionSubmitRespondApiModel>(resultStr);
            return respond;
        }

        private string GetTimestamp(DateTime time)
        {
            //1470441600 (2016-08-06 00:00:00 UTC)
            //1470603600 (2016-08-07 21:00:00 UTC)
            if (time.Kind != DateTimeKind.Utc) throw new InvalidOperationException("time not utc");
            Int32 unixTimestamp = (Int32) time.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
            return unixTimestamp.ToString();
        }


        public SolutionSubmitRespondApiModel SubmitSolution(Int32 problemId, SolutionModel solution)
        {
            SolutionFormatter f = new SolutionFormatter();
            var solutionStr =f.GetSolutionSpecString(solution);
            return SubmitSolution(problemId, solutionStr);
        }

        public SolutionSubmitRespondApiModel SubmitProblem(DateTime time, SolutionModel solution)
        {
            SolutionFormatter f = new SolutionFormatter();
            var solutionStr = f.GetSolutionSpecString(solution);
            return SubmitProblem(time, solutionStr);
        }

        public SolutionSubmitRespondApiModel SubmitProblemForNextTime(SolutionModel solution, int skipHours = 1)
        {
            var utcnow = DateTime.UtcNow.AddHours(skipHours);
            var nextTime = new DateTime(utcnow.Year, utcnow.Month, utcnow.Day, utcnow.Hour, 0, 0, DateTimeKind.Utc);

            return SubmitProblem(nextTime, solution);
        }

        public T GetBlob<T>(string hash)
        {
            var blob = GetBlob(hash);
            var model = JsonConvert.DeserializeObject<T>(blob);
            return model;
        }

        public String GetBlob(string hash)
        {
            var c = GetClient();
            var zippedRespond = c.DownloadData("http://2016sv.icfpcontest.org/api/blob/" + hash);
            var respond = Unzip(zippedRespond);
            return respond;
        }


        public SnapshotBriefApiModel[] GetSnapshots()
        {
            var c = GetClient();
            var respondSnapshotsZipped = c.DownloadData("http://2016sv.icfpcontest.org/api/snapshot/list");
            var respondSnapshots = Unzip(respondSnapshotsZipped);
            var snapshots = JsonConvert.DeserializeObject<SnapshotListApiModel>(respondSnapshots);
            return snapshots.snapshots;
        }



        #region inner tools


        private byte[] FormatForm(Dictionary<string, string> content)
        {
            StringBuilder sb = new StringBuilder();

            bool isFirst = true;
            foreach (var el in content)
            {
                if (!isFirst)
                    sb.Append("&");
                else
                    isFirst = false;

                sb.Append(HttpUtility.UrlEncode(el.Key));
                sb.Append("=");
                sb.Append(HttpUtility.UrlEncode(el.Value));
            }

            var data = Encoding.ASCII.GetBytes(sb.ToString());
            return data;
        }


        private WebClient _c;
        WebClient GetClient()
        {
            if (_c == null)
            {
                _c = new WebClient();
                _c.Headers.Add("X-API-Key", ApiKey);
                _c.Headers.Add("Accept-Encoding", "gzip");
            }
            return _c;
        }



        static String Unzip(Byte[] zippedRespond)
        {
            using (GZipStream zip = new GZipStream(new MemoryStream(zippedRespond), CompressionMode.Decompress))
            {
                var r = new StreamReader(zip);
                var respond = r.ReadToEnd();
                return respond;
            }
        }
        #endregion
    }


}
