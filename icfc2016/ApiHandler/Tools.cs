﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using ApiHandler.ApiModels;
using Newtonsoft.Json;
using NLog;
using NUnit.Framework;
using Platform;
using Platform.Models;
using Platform.SolverByFolding;

namespace ApiHandler
{
    [TestFixture, Explicit]
    public class Tools
    {
        private static readonly Logger Log = LogManager.GetLogger("TOOLS");

        private string solutionDir = @"d:\Projects\icfpc2016";

        [Test]
        public void WriteAllTasksToFiles()
        {
            var api = new ApiHandler();

            var snapshots = api.GetSnapshots();
            var lastSnapshotHash = snapshots.OrderByDescending(x => x.snapshot_time).First().snapshot_hash;

            Thread.Sleep(1010);

            var snapshot = api.GetBlob<SnapshotApiModel>(lastSnapshotHash);

            Thread.Sleep(1010);

            var problemsDir = solutionDir +@"\icfc2016\FolderTool\Tasks\";

            foreach (var problem in snapshot.problems.OrderBy(p => p.problem_id))
            {
                var existing = Directory.GetFiles(problemsDir, $"problem_{problem.problem_id}_*.json");
                if (existing.Any())
                    continue;

                var problemSpec = api.GetBlob(problem.problem_spec_hash);
                File.WriteAllText(problemsDir+$"problem_{problem.problem_id}_{problem.problem_spec_hash}.json", problemSpec);
                Thread.Sleep(200);
            }
        }


        [Test]
        public void SaveSnapshot()
        {
            var api = new ApiHandler();

            var snapshots = api.GetSnapshots();
            var lastSnapshotHash = snapshots.OrderByDescending(x => x.snapshot_time).First().snapshot_hash;

            Thread.Sleep(1010);

            var snapshot = api.GetBlob(lastSnapshotHash);

            File.WriteAllText(solutionDir + @"\LastSnapshot.json", snapshot);

        }



        [Test]
        public void SubmitSolution()
        {
            var  solution = DemoTasks.demoTask3SolutionModel;
            var problemId = 3;

            var api = new ApiHandler();

            var respond = api.SubmitSolution(problemId, solution);

            Assert.IsTrue(respond.ok);
        }


        [Test]
        public void SubmitProblem()
        {
            var solution = DemoTasks.demoTask3SolutionModel;
            var utcnow = DateTime.UtcNow.AddHours(1);
            var nextTime = new DateTime(utcnow.Year, utcnow.Month, utcnow.Day, utcnow.Hour, 0 ,0, DateTimeKind.Utc);

             var api = new ApiHandler();

            var respond = api.SubmitProblem(nextTime, solution);

            Assert.IsTrue(respond.ok);

        }

       

        [Test]
        public void SolveAll()
        {
            var problems = GetProblems().OrderByDescending(x => x.ProblemId).ToList();

            var solvedproblemsInfoFile = solutionDir + @"\solvedProblemsInfo.json";
            var solvedproblemsInfoTxt = File.ReadAllText(solvedproblemsInfoFile);

            var solvedproblemsInfo = JsonConvert.DeserializeObject<List<SolutionSubmitRespondApiModel>>(solvedproblemsInfoTxt);

            SolverFolder solver = new SolverFolder();
            ApiHandler h = new ApiHandler();
            foreach (var problemModel in problems)
            {
                var solvingInfo = solvedproblemsInfo.FirstOrDefault(x => x.problem_id == problemModel.ProblemId);

                //// из за старого бага в бд папали resemblance более 1. жаль. надо пересчитывать их вероятно
                //if (solvingInfo!= null  &&  solvingInfo.resemblance > 0.9 && solvingInfo.resemblance <= 1)
                //    continue;

                // пропускаем всё что решали ранее
                if (solvingInfo != null)
                    continue;

                try
                {
                    var result = solver.SolveProblem(problemModel);
                    var respond = h.SubmitSolution(problemModel.ProblemId, result.Solution);

                    if (respond.ok)
                    {
                        var solvingInfo2 = solvedproblemsInfo.FirstOrDefault(x => x.problem_id == respond.problem_id);
                        if (solvingInfo2 != null)
                            solvedproblemsInfo.Remove(solvingInfo2);
                        respond.Generation = 21;
                        solvedproblemsInfo.Add(respond);

                        File.WriteAllText(solvedproblemsInfoFile, JsonConvert.SerializeObject(solvedproblemsInfo, Formatting.Indented));

                        Log.Info(
                            $"Problem {problemModel.ProblemId} solved! Resemplance:{respond.resemblance} Size:{respond.solution_size}");
                    }
                    else
                    {
                        Log.Warn($"Problem {problemModel.ProblemId} NOT solved. ok=false!");
                    }
                    
                }

                catch (Exception ex)
                {
                    Log.Warn(ex, $"Problem {problemModel.ProblemId} NOT solved: " + ex.Message);
                }

            }
            

        }

        private List<ProblemModel> GetProblems()
        {
            var problemsDir = solutionDir + @"\icfc2016\FolderTool\Tasks";
            var problemFiles = Directory.GetFiles(problemsDir, "*.json");
            
            var models = new List<ProblemModel>();

            foreach (var problemFile in problemFiles)
            {
                // problem_2519_05a6c69e1d574178c3a36822a764b78bcb07b898.json
                var problemText = File.ReadAllText(problemFile);

                var m = Regex.Match(problemFile, "problem_(\\d+)_");
                if (!m.Success)
                {
                    Log.Warn("invalid file");
                    continue;
                }
                var id = Int32.Parse(m.Groups[1].Value);

                var model = ProblemParser.ParseProblem(problemText);
                model.ProblemId = id;
                models.Add(model);
            }
            return models;
        }
    }
}