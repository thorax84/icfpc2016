using Fractions;

namespace Platform
{
    public static class FractionMath
    {
        public static Fraction Min(Fraction a, Fraction b)
        {
            return a < b ? a : b;
        }

        public static Fraction Max(Fraction a, Fraction b)
        {
            return a > b ? a : b;
        }
    }
}