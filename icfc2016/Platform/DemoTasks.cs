using System.Collections.Generic;
using Fractions;
using Platform.Geometry;
using Platform.Models;

namespace Platform
{
    public static class DemoTasks
    {

        public static string demoTask1 = @"1
4
0,0
1,0
1,1
0,1
4
0,0 1,0
0,0 0,1
1,0 1,1
0,1 1,1";

        public static string demoTask1Solution = @"4
0,0
1,0
1,1
0,1
1
4 0 1 2 3
0,0
1,0
1,1
0,1";

        public static string demoTask2 = @"1
4
1,1
2,1
2,2
1,2
4
1,1 1,2
2,1 2,2
1,1 2,1
1,2 2,2";

        public static string demoTask2Solution =@"4
0,0
1,0
1,1
0,1
1
4 0 1 2 3
1,1
2,1
2,2
1,2";
        public static string demoTask3 = @"1
4
-1/2,-1/2
1/2,-1/2
1/2,1/2
-1/2,1/2
4
-1/2,-1/2 1/2,-1/2
-1/2,-1/2 -1/2,1/2
1/2,-1/2 1/2,1/2
-1/2,1/2 1/2,1/2";

        public static SolutionModel demoTask3SolutionModel = new SolutionModel()
        {
            Points = new List<SolutionPoint>
            {
                new SolutionPoint {Src = new Vector(0.0, 0.0), Dest = new Vector("-1/2", "-1/2")},
                new SolutionPoint {Src = new Vector(1.0, 0.0), Dest = new Vector("1/2", "-1/2")},
                new SolutionPoint {Src = new Vector(1.0, 1.0), Dest = new Vector("1/2", "1/2")},
                new SolutionPoint {Src = new Vector(0.0, 1.0), Dest = new Vector("-1/2", "1/2")},
            },
            Polygons = new List<List<int>>
            {
                new List<int> {0, 1, 2, 3}
            }
        };



    }
}