﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using Fractions;
using Platform.Geometry;

namespace Platform.Models
{
    //[DebuggerDisplay("{X},{Y}")]
    //public class IcfpPoint
    //{
    //    public IcfpPoint(Fraction x, Fraction y)
    //    {
    //        X = x;
    //        Y = y;
    //    }
    //    public IcfpPoint(string x, string y)
    //    {
    //        X = Fraction.FromString(x);
    //        Y = Fraction.FromString(y);
    //    }

    //    public IcfpPoint(Vector v)
    //    {
    //        X = v.X;
    //        Y = v.Y;
    //    }

    //    public Fraction X { get; set; }
    //    public Fraction Y { get; set; }
    //}

    //[DebuggerDisplay("{From.X},{From.Y} - {To.X},{To.Y}")]
    //public class IcfpSegment
    //{
    //    public IcfpSegment(Fraction fromX, Fraction fromY, Fraction toX, Fraction toY)
    //    {
    //        From = new IcfpPoint(fromX, fromY);
    //        To = new IcfpPoint(toX, toY);
    //    }

    //    public IcfpPoint From { get; set; }
    //    public IcfpPoint To { get; set; }
    //}

    [DebuggerDisplay("{Verticles.Length} points")]
    public class IcfpPolygon
    {
        private Vector[] _verticles;

        public Vector[] Verticles   
        {
            get { return _verticles; }
            set
            {
                _verticles = value;
                CalcGetEdges();
            }
        }

        public Line[] Edges { get; private set; }

        public void CalcGetEdges()
        {
            var first = Verticles[0];

            var ret = new List<Line>();

            var last = first;
            for (var i = 1; i < Verticles.Length; i++)
            {
                var cur = Verticles[i];
                ret.Add(new Line(last, cur));
                last = cur;
            }

            ret.Add(new Line(last, first));

            Edges = ret.ToArray();
        }

        public IcfpPolygon Transform(Matrix transformation)
        {
            var newVerticles  = Verticles.Select(x => transformation*x).ToArray();
            return new IcfpPolygon {Verticles = newVerticles};
        }
    }

    public class IcfcRect
    {
        public Fraction L { get; set; }
        public Fraction R { get; set; }
        public Fraction T { get; set; }
        public Fraction B { get; set; }

        public Vector Bl => new Vector(L,B);
        public Fraction Width => (R - L).Abs();
        public Fraction Height => (T - B).Abs();

        public bool IsIn(IcfcRect bounding)
        {
            return IsInOrOnEdge(this, bounding);
        }

        public Fraction GetSquare()
        {
            return Width * Height;
        }

        /// <summary>
        /// Единичный квадрат
        /// </summary>
        public static IcfcRect Simple = new IcfcRect {L=0, T=1,B=0, R=1};

        public static bool IsInOrOnEdge(IcfcRect inner, IcfcRect bounding)
        {
            return inner.L >= bounding.L &&
                   inner.R <= bounding.R &&
                   inner.T <= bounding.T &&
                   inner.B >= bounding.B;
        }


    }

}