using System;
using System.Collections.Generic;
using System;
using Platform.Geometry;

namespace Platform.Models
{
    public class SolutionModel
    {
        public List<SolutionPoint> Points { get; set; }
        public List<List<Int32>> Polygons { get; set; }

        public void Transform(Matrix mx)
        {
            for (int i = 0; i < Points.Count; i++)
            {
                Points[i].Dest = mx * Points[i].Dest;
            }
        }
    }

    public class SolutionPoint
    {
        /// <summary>
        /// ����� ��������� �����, � ������ ���������� ��������
        /// </summary>
        public Vector Src { get; set; }
        /// <summary>
        /// ����� �������
        /// </summary>
        public Vector Dest { get; set; }
    }
}