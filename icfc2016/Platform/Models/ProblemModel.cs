using System;
using Platform.Geometry;

namespace Platform.Models
{
    public class ProblemModel
    {
        public Int32 ProblemId { get; set; }
        public IcfpPolygon[] Silhouette { get; set; }
        public Line[] Skeleton { get; set; }

        public void Transform(Matrix mx)
        {
            for(int i = 0;i < Silhouette.Length;i++)
            {
                Silhouette[i] = Silhouette[i].Transform(mx);
            }
        }
    }
}