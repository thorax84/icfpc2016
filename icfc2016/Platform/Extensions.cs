﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform
{
    public static class Extensions
    {
        public static Int32 ReadIntLine(this TextReader r)
        {
            var line = r.ReadLine();
            return Int32.Parse(line);
        }
    }
}
