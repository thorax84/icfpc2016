﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Fractions;
using Platform.Geometry;
using Platform.Models;

namespace Platform
{
    public static  class ProblemParser
    {
        public static ProblemModel ParseProblem(string problem)
        {
            StringReader r = new StringReader(problem);

            // parse silhouette
            var polyCount = r.ReadIntLine();

            List<IcfpPolygon> poligons = new List<IcfpPolygon>();
            for (int i = 0; i < polyCount; i++)
            {
                var poly = GetPolygon(r);
                poligons.Add(poly);
            }

            // parse skeleton
            var skeletonSegmentCount = r.ReadIntLine();

            List<Line> skeletonSegments = new List<Line>();
            for (int i = 0; i < skeletonSegmentCount; i++)
            {
                var seg = GetSegment(r);
                skeletonSegments.Add(seg);
            }

            return new ProblemModel
            {
                Silhouette =  poligons.ToArray(),
                Skeleton = skeletonSegments.ToArray(),
            };
        }

        private static Line GetSegment(StringReader r)
        {
            // 0,0 0,1
            // 1/2,1/2 0,1/2
            var val = r.ReadLine();
            var points = val.Split(' ');
            var fromCoords = points[0].Split(',');
            var toCoords = points[1].Split(',');

            Fraction fromX, fromY, toX, toY;
            Fraction.TryParse(fromCoords[0], out fromX);
            Fraction.TryParse(fromCoords[1], out fromY);
            Fraction.TryParse(toCoords[0], out toX);
            Fraction.TryParse(toCoords[1], out toY);

            return new Line(fromX, fromY, toY, toY);
        }

        private static IcfpPolygon GetPolygon(StringReader r)
        {
            var vertexCount = r.ReadIntLine();
            List<Vector> vertices = new List<Vector>();
            for (int i = 0; i < vertexCount; i++)
            {
                var vertex = GetVertex(r);
                vertices.Add(vertex);
            }
            return new IcfpPolygon { Verticles = vertices.ToArray() };
        }

        private static Vector GetVertex(StringReader r)
        {
            var vertexStr = r.ReadLine();
            var coords = vertexStr.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var x = ParseIcfcFloat(coords[0]);
            var y = ParseIcfcFloat(coords[1]);

            return new Vector(x, y);
        }

        private static Fraction ParseIcfcFloat(string str)
        {
            Fraction result;
            Fraction.TryParse(str, NumberStyles.Integer, null, out result);
            return result;
        }
    }
}