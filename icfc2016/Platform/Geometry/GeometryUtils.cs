﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Fractions;
using Platform.Models;

namespace Platform.Geometry
{
    public static class GeometryUtils
    {
        public static Vector IntersectLines(Vector p1, Vector p2, Vector p3, Vector p4)
        {
            var deter = (p1.X - p2.X) * (p3.Y - p4.Y) - (p1.Y - p2.Y) * (p3.X - p4.X);
            var px = (p1.X * p2.Y - p1.Y * p2.X) * (p3.X - p4.X) - (p1.X - p2.X) * (p3.X * p4.Y - p3.Y * p4.X);
            var py = (p1.X * p2.Y - p1.Y * p2.X) * (p3.Y - p4.Y) - (p1.Y - p2.Y) * (p3.X * p4.Y - p3.Y * p4.X);

            return new Vector(px / deter, py / deter);
        }

        public static Vector GetCenterOfPointsSet(List<Vector> points)
        {
            double summX = 0, summY = 0;
            foreach(Vector p in points)
            {
                summX += p.X.ToDouble();
                summY += p.Y.ToDouble();
            }

            var centerX = summX / points.Count;
            var centerY = summY / points.Count;

            centerX = Math.Truncate(centerX * 1000) / 1000;
            centerY = Math.Truncate(centerY * 1000) / 1000;

            return new Vector(new Fraction(centerX), new Fraction(centerY));
        }

        public static List<Vector> ComputeConvexHull(List<Vector> points, bool sortInPlace = false)
        {
            if (!sortInPlace)
                points = new List<Vector>(points);
            points.Sort((a, b) =>
                a.X == b.X ? a.Y.CompareTo(b.Y) : a.X.CompareTo(b.X));

            // Importantly, DList provides O(1) insertion at beginning and end
            List<Vector> hull = new List<Vector>();
            int L = 0, U = 0; // size of lower and upper hulls

            // Builds a hull such that the output polygon starts at the leftmost point.
            for (int i = points.Count - 1; i >= 0; i--)
            {
                Vector p = points[i], p1;

                // build lower hull (at end of output list)
                while (L >= 2 && (p1 = hull.Last()).Sub(hull[hull.Count - 2]).Cross(p.Sub(p1)) >= 0)
                {
                    hull.RemoveAt(hull.Count - 1);
                    L--;
                }
                hull.Add(p);
                L++;

                // build upper hull (at beginning of output list)
                while (U >= 2 && (p1 = hull[0]).Sub(hull[1]).Cross(p.Sub(p1)) <= 0)
                {
                    hull.RemoveAt(0);
                    U--;
                }
                if (U != 0) // when U=0, share the point added above
                    hull.Insert(0, p);
                U++;
                Debug.Assert(U + L == hull.Count + 1);
            }
            hull.RemoveAt(hull.Count - 1);
            return hull;
        }

        public static IcfcRect GetBoundRect(List<Vector> points)
        {
            IcfcRect ret = new IcfcRect();
            var forstVerticle = points[0];
            ret.L = ret.R = forstVerticle.X;
            ret.T = ret.B = forstVerticle.Y;

            foreach (var point in points)
            {
                ret.L = FractionMath.Min(point.X, ret.L);
                ret.R = FractionMath.Max(point.X, ret.R);
                ret.T = FractionMath.Max(point.Y, ret.T);
                ret.B = FractionMath.Min(point.Y, ret.B);
            }

            return ret;
        }
    }
}
