﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fractions;
using System.Diagnostics;
using Platform.Models;

namespace Platform.Geometry
{
    public class Folder
    {
        List<PointInfo> Points;
        List<Polygon> Polies;
        List<PolyEdge> Edges;

        public Folder()
        {
            Reset();
        }

        public void Reset()
        {
            Points = new List<PointInfo>();
            Polies = new List<Polygon>();
            Edges = new List<PolyEdge>();

            Points.Add(new PointInfo() { Src = new Vector(0.0, 0.0), Dst = new Vector(0.0, 0.0), Mx = Matrix.CreateIdentity(), IsSquare = true });
            Points.Add(new PointInfo() { Src = new Vector(1.0, 0.0), Dst = new Vector(1.0, 0.0), Mx = Matrix.CreateIdentity(), IsSquare = true });
            Points.Add(new PointInfo() { Src = new Vector(1.0, 1.0), Dst = new Vector(1.0, 1.0), Mx = Matrix.CreateIdentity(), IsSquare = true });
            Points.Add(new PointInfo() { Src = new Vector(0.0, 1.0), Dst = new Vector(0.0, 1.0), Mx = Matrix.CreateIdentity(), IsSquare = true });

            var square = new Polygon()
            {
                Edges = null,
                Mx = Matrix.CreateIdentity()
            };

            Polies.Add(square);

            Edges.Add(new PolyEdge() { PolyLeft = square, P1 = Points[0], P2 = Points[1] });
            Edges.Add(new PolyEdge() { PolyLeft = square, P1 = Points[1], P2 = Points[2] });
            Edges.Add(new PolyEdge() { PolyLeft = square, P1 = Points[2], P2 = Points[3] });
            Edges.Add(new PolyEdge() { PolyLeft = square, P1 = Points[3], P2 = Points[0] });

            square.Edges = new List<PolyEdge>(Edges);
        }

        public void Fold(Line line)
        {
            UpdatePointsSideOfLine(line);
            SplitEdgesByLine(line);
            //CheckPoliesEdges();

            DetectPoliesCrossingLine();

            //CheckPoliesEdges();

            SplitPoliesByLine();
            UpdatePointsAndPolies(line);

            //CheckPoliesEdges();
        }

        public void FoldRandom(int itersCount)
        {
            var rnd = new Random();
            for (int i = 0;i < itersCount;i++)
            {
                int edgeIdx1 = rnd.Next(Edges.Count);
                int edgeIdx2;

                for (;;)
                {
                    edgeIdx2 = rnd.Next(Edges.Count);
                    if (edgeIdx1 != edgeIdx2)
                        break;
                }

                var k1 = new Fraction(1, rnd.Next(5) + 1);
                var k2 = new Fraction(1, rnd.Next(5) + 1);

                var edge1 = Edges[edgeIdx1];
                var edge2 = Edges[edgeIdx2];

                var a = edge1.P1.Dst + k1 * (edge1.P2.Dst - edge1.P1.Dst);
                var b = edge2.P1.Dst + k2 * (edge2.P2.Dst - edge2.P1.Dst);

                Fold(new Line(a, b));
            }
        }

        public void CalcSrc()
        {
            foreach (var pointInfo in Points)
            {
                pointInfo.Src = pointInfo.Mx.Invert() * pointInfo.Dst;
            }
        }

        public SolutionModel GetSolution()
        {
            CalcSrc();

            SolutionModel solution = new SolutionModel();
            solution.Points = new List<SolutionPoint>();
            solution.Polygons = new List<List<int>>();

            foreach (var point in Points)
            {
                solution.Points.Add(new SolutionPoint()
                {
                    Src = point.Src,
                    Dest = point.Dst
                });                
            }

            foreach(var poly in Polies)
            {
                var pointIndexes = new List<int>();

                for(int i = 0;i < poly.PointsCount();i++)
                {
                    pointIndexes.Add(
                        Points.IndexOf(poly.GetPoint(i)));
                }

                solution.Polygons.Add(pointIndexes);
            }

            return solution;
        }

        void UpdatePointsSideOfLine(Line line)
        {
            foreach (var pointInfo in Points)
            {
                pointInfo.Side = line.GetSideOfLine(pointInfo.Dst);
            }
        }

        void UpdatePointsAndPolies(Line line)
        {
            var reflectMx = Matrix.CreateReflect(line);

            foreach (var pointInfo in Points)
            {
                if (pointInfo.Side == SideOfLine.AtLeft)
                {
                    pointInfo.Mx = reflectMx * pointInfo.Mx;
                    pointInfo.Dst = reflectMx * pointInfo.Dst;

                    //pointInfo.CheckSrc();
                }
            }
            
            foreach (var poly in Polies)
            {                
                if (poly.IsAtLeft())
                {
                    poly.Mx = reflectMx * poly.Mx;
                }
            }
        }

        void CheckPoliesEdges()
        {
            int idx = 0;
            foreach (var poly in Polies)
            {               
                poly.CheckEdgesLength();
                idx++;
            }
        }

        void SplitEdgesByLine(Line line)
        {
            foreach (var poly in Polies)
            {
                poly.IsCrossLine = false;
            }

            //var newEdges = new List<PolyEdge>();

            int i = 0;
            foreach (var edge in new List<PolyEdge>(Edges))
            {
                if (edge.GetSideOfLine() == SideOfLine.CrossLine)
                {
                    //CheckPoliesEdges();
                    
                    var point1 = edge.P1;
                    var point2 = edge.P2;

                    var newPoint = GeometryUtils.IntersectLines(point1.Dst, point2.Dst, line.A, line.B);

                    var newPointInfo = new PointInfo()
                    {
                        Dst = newPoint,
                        Side = SideOfLine.OnLine,
                        Mx = edge.PolyLeft.Mx
                    };

                    ////////////////
                    //if (i == 2 && Polies.IndexOf(edge.PolyLeft) == 3)
                    //{
                    //    var its_src1 = edge.P1.Mx.Invert() * edge.P1.Dst;
                    //    var its_src2 = edge.P2.Mx.Invert() * edge.P2.Dst;

                    //    var polyMx = edge.PolyLeft.Mx;
                    //    polyMx = polyMx.Invert();
                    //    var bypoly_src1 = polyMx * edge.P1.Dst;
                    //    var bypoly_src2 = polyMx * edge.P2.Dst;
                    //}
                    ///////////////

                    //point1.CheckSrc();
                    //point2.CheckSrc();
                    //newPointInfo.CheckSrc();

                    Points.Add(newPointInfo);

                    var newEdge = new PolyEdge()
                    {
                        PolyLeft = edge.PolyLeft,
                        PolyRight = edge.PolyRight,
                        P1 = newPointInfo,
                        P2 = edge.P2
                    };
                    
                    edge.P2 = newPointInfo;

                    Edges.Add(newEdge);

                    edge.PolyLeft.AddEdgeAfter(edge, newEdge);
                    edge.PolyLeft.IsCrossLine = true;

                    if (edge.PolyRight != null)
                    {
                        edge.PolyRight.AddEdgeBefore(edge, newEdge);
                        edge.PolyRight.IsCrossLine = true;
                    }

                    //CheckPoliesEdges();                    
                }

                i++;
            }

            //Edges.AddRange(newEdges);
        }

        void DetectPoliesCrossingLine()
        {
            foreach (var poly in Polies)
            {
                bool hasAtLeft = false;
                bool hasAtRight = false;

                poly.IsCrossLine = false;

                foreach (var point in poly.GetPoints())
                {
                    if (point.Side == SideOfLine.AtLeft)
                        hasAtLeft = true;

                    if (point.Side == SideOfLine.AtRight)
                        hasAtRight = true;                    

                    if(hasAtLeft && hasAtRight)
                    {
                        poly.IsCrossLine = true;
                        break;
                    }
                }                
            }
        }

        void SplitPoliesByLine()
        {
            var newPolies = new List<Polygon>();

            foreach (var poly in Polies)
            {
                if (poly.IsCrossLine)
                {
                    int changeIdx1 = -1;
                    int changeIdx2 = -1;

                    var edgesCount = poly.Edges.Count;
                    for (int i = 0; i < edgesCount; i++)
                    {
                        var nextI = (i + 1) % edgesCount;

                        var edge = poly.Edges[i];
                        Debug.Assert(edge.GetSideOfLine() != SideOfLine.CrossLine);

                        var nextEdge = poly.Edges[nextI];

                        var side = edge.GetSideOfLine();
                        if (side != nextEdge.GetSideOfLine())
                        {
                            if (changeIdx1 == -1)
                                changeIdx1 = i;
                            else changeIdx2 = i;
                        }
                    }

                    Debug.Assert(changeIdx1 != -1);
                    Debug.Assert(changeIdx2 != -1);
                    Debug.Assert(Math.Abs(changeIdx1 - changeIdx2) > 1);

                    var newRightPoly = new Polygon();
                    newRightPoly.Mx = poly.Mx;
                    newPolies.Add(newRightPoly);

                    var newEdge = new PolyEdge()
                    {
                        PolyLeft = poly,
                        PolyRight = newRightPoly,
                        P1 = poly.GetPoint(changeIdx1 + 1),
                        P2 = poly.GetPoint((changeIdx2 + 1) % edgesCount)
                    };


                    Edges.Add(newEdge);
                    newRightPoly.Edges.Add(newEdge);

                    int startMoveEdgeIdx = changeIdx1 + 1;
                    int endMoveEdgeIdx = changeIdx2;

                    for (int i = startMoveEdgeIdx; i <= endMoveEdgeIdx; i++)
                    {
                        var edgeToMove = poly.Edges[i];
                        newRightPoly.Edges.Add(edgeToMove);

                        if (edgeToMove.PolyLeft == poly)
                            edgeToMove.PolyLeft = newRightPoly;
                        else
                            edgeToMove.PolyRight = newRightPoly;
                    }

                    poly.Edges.RemoveRange(startMoveEdgeIdx, endMoveEdgeIdx - startMoveEdgeIdx + 1);
                    poly.Edges.Insert(startMoveEdgeIdx, newEdge);
                }
            }

            Polies.AddRange(newPolies);
        }



        [DebuggerDisplay("{P1.Dst.X},{P1.Dst.Y} - {P2.Dst.X},{P2.Dst.Y}")]
        class PolyEdge
        {
            public Polygon PolyLeft;
            public Polygon PolyRight;
            public PointInfo P1;
            public PointInfo P2;
            
            public SideOfLine GetSideOfLine()
            {
                if (P1.Side == SideOfLine.OnLine)                
                    return P2.Side;                
                else if (P2.Side == SideOfLine.OnLine)                
                    return P1.Side;

                if (P1.Side != P2.Side)
                    return SideOfLine.CrossLine;

                return P1.Side;
            }
        }

        class Polygon
        {
            public List<PolyEdge> Edges = new List<PolyEdge>();
            public bool IsCrossLine;
            public Matrix Mx = Matrix.CreateIdentity();

            public void AddEdgeAfter(PolyEdge after, PolyEdge edge)
            {
                Edges.Insert(Edges.IndexOf(after) + 1, edge);
            }

            public void AddEdgeBefore(PolyEdge before, PolyEdge edge)
            {
                Edges.Insert(Edges.IndexOf(before), edge);
            }

            public PolyEdge GetEdgeByModIdx(int idx)
            {
                return Edges[idx % Edges.Count];
            }

            public int PointsCount()
            {
                return Edges.Count;
            }

            public bool IsAtLeft()
            {
                return GetPoints().Any(p => p.Side == SideOfLine.AtLeft);
            }

            public PointInfo GetPoint(int index)
            {
                var edge = Edges[index];
                if (edge.PolyLeft == this) return edge.P1;
                return edge.P2;
            }

            public IEnumerable<PointInfo> GetPoints()
            {
                for(int i = 0;i < Edges.Count;i++)
                {
                    yield return GetPoint(i);
                }
            }

            public void CheckEdgesLength()
            {
                int idx = 0;
                foreach (var edge in Edges)
                {
                    var invMx1 = edge.P1.Mx.Invert();
                    var invMx2 = edge.P2.Mx.Invert();

                    var d1 = edge.P1.Dst;
                    var d2 = edge.P2.Dst;

                    var s1 = invMx1 * edge.P1.Dst;
                    var s2 = invMx2 * edge.P2.Dst;

                    Debug.Assert((d1 - d2).GetLenSq() == (s1 - s2).GetLenSq());

                    idx += 1;
                }
            }
        }

        [DebuggerDisplay("{Dst.X},{Dst.Y};{Side}")]
        class PointInfo
        {
            public Vector Src;
            public Vector Dst;
            public Matrix Mx;
            public SideOfLine Side;
            public bool IsSquare;

            public void CheckSrc()
            {
                var src = Mx.Invert() * Dst;

                Debug.Assert(src.X >=0 && src.X <= 1 && src.Y >=0 && src.Y <= 1);
            }
        }
    }
}
