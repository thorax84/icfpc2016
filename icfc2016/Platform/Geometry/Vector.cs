﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Fractions;

namespace Platform.Geometry
{
    public enum SideOfLine
    {
        AtLeft,
        OnLine,
        AtRight,
        CrossLine
    }

    [DebuggerDisplay("{X},{Y}")]
    public struct Vector
    {
        public Fraction X, Y;

        public Vector(double X, double Y)
        {
            this.X = new Fraction(X);
            this.Y = new Fraction(Y);
        }


        public Vector(Fraction x, Fraction y)
        {
            this.X = x; this.Y = y;
        }

        public Vector(string x, string y)
        {
            X = Fraction.FromString(x);
            Y = Fraction.FromString(y);
        }

        public Vector GetInvert()
        {
            return new Vector(X.Invert(),Y.Invert());
        }


        public Vector GetOrth()
        {
            return new Vector(0-Y, X);
        }        

        public Fraction GetLenSq()
        {
            return X * X + Y * Y;
        }

        public Fraction Dot(Vector v2)
        {
            return X * v2.X + Y * v2.Y;
        }

        public static bool operator ==(Vector v1, Vector v2)
        {
            return v1.X == v2.X && v1.Y == v2.Y;
        }

        public static bool operator !=(Vector v1, Vector v2)
        {
            return !(v1 == v2);
        }

        public static Vector operator +(Vector v1, Vector v2)
        {
            return new Vector(
                v1.X + v2.X,
                v1.Y + v2.Y);
        }

        public static Vector operator *(Fraction d, Vector v2)
        {
            return new Vector(
                d * v2.X,
                d * v2.Y);
        }

        public static Vector operator -(Vector v1, Vector v2)
        {
            return new Vector(
                v1.X - v2.X,
                v1.Y - v2.Y);
        }

        public static Vector operator -(Vector v)
        {
            return new Vector(0-v.X, 0-v.Y);
        }

        public Vector Sub(Vector v)
        {
            return new Vector(X - v.X, Y - v.Y);
        }

        public Fraction Cross(Vector right)
        {
            return X * right.Y - Y * right.X;
        }
    }

    [DebuggerDisplay("{A.X},{A.Y} - {B.X},{B.Y}")]
    public struct Line
    {
        public Vector A, B, AB, Orth;

        public Line(Vector a, Vector b)
        {
            A = a;
            B = b;
            AB = B - A;
            Orth = (b - a).GetOrth();
        }

        public Line(Fraction aX, Fraction aY, Fraction bX, Fraction bY) : this(new Vector(aX,aY), new Vector(bX,bY) )
        {

        }

        public SideOfLine GetSideOfLine(Vector v)
        {
            var d = (v - A).Dot(Orth);
            if (d == 0) return SideOfLine.OnLine;
            if (d > 0) return SideOfLine.AtLeft;
            return SideOfLine.AtRight;
        }
    }
    
    public struct Matrix
    {
        public Fraction
            A11, A12, A13,
            A21, A22, A23,
            A31, A32, A33;        
        
        public Matrix Invert()
        {
            var determinant =
                A11 * A22 * A33 + A21 * A32 * A13 + A31 * A12 * A23 - A11 * A32 * A23 - A31 * A22 * A13 - A21 * A12 * A33;

            var invdet = 1 / determinant;

            Matrix result = new Matrix();

            result.A11 = (A22 * A33 - A32 * A23) * invdet;
            result.A12 = (A13 * A32 - A12 * A33) * invdet;
            result.A13 = (A12 * A23 - A13 * A22) * invdet;

            result.A21 = (A23 * A31 - A21 * A33) * invdet;
            result.A22 = (A11 * A33 - A13 * A31) * invdet;
            result.A23 = (A21 * A13 - A11 * A23) * invdet;

            result.A31 = (A21 * A32 - A31 * A22) * invdet;
            result.A32 = (A31 * A12 - A11 * A32) * invdet;
            result.A33 = (A11 * A22 - A21 * A12) * invdet;

            return result;
        }

        public static Matrix operator *(Matrix m1, Matrix m2)
        {
            return new Matrix()
            {
                A11 = m1.A11 * m2.A11 + m1.A12 * m2.A21 + m1.A13 * m2.A31,
                A12 = m1.A11 * m2.A12 + m1.A12 * m2.A22 + m1.A13 * m2.A32,
                A13 = m1.A11 * m2.A13 + m1.A12 * m2.A23 + m1.A13 * m2.A33,

                A21 = m1.A21 * m2.A11 + m1.A22 * m2.A21 + m1.A23 * m2.A31,
                A22 = m1.A21 * m2.A12 + m1.A22 * m2.A22 + m1.A23 * m2.A32,
                A23 = m1.A21 * m2.A13 + m1.A22 * m2.A23 + m1.A23 * m2.A33,

                A31 = m1.A31 * m2.A11 + m1.A32 * m2.A21 + m1.A33 * m2.A31,
                A32 = m1.A31 * m2.A12 + m1.A32 * m2.A22 + m1.A33 * m2.A32,
                A33 = m1.A31 * m2.A13 + m1.A32 * m2.A23 + m1.A33 * m2.A33,
            };
        }

        public static Vector operator *(Matrix m, Vector v)
        {
            return new Vector(
                m.A11 * v.X + m.A12 * v.Y + m.A13,
                m.A21 * v.X + m.A22 * v.Y + m.A23);
        }

        public static Matrix CreateReflect(Vector dir)
        {
            var lengthSq = dir.GetLenSq();
            return new Matrix()
            {
                A11 = (dir.X * dir.X - dir.Y * dir.Y) / lengthSq,
                A12 = (2 * dir.X * dir.Y) / lengthSq,
                A21 = (2 * dir.X * dir.Y) / lengthSq,
                A22 = (dir.Y * dir.Y - dir.X * dir.X) / lengthSq,
                A33 = 1
            };
        }

        public static Matrix CreateMove(Vector dir)
        {
            return new Matrix()
            {
                A11 = 1,
                A22 = 1,
                A13 = dir.X,
                A23 = dir.Y,
                A33 = 1
            };
        }

        public static Matrix CreateRotation(Fraction sin, Fraction cos)
        {
            return new Matrix()
            {
                A11 = cos, A12 = 0-sin,
                A21 = sin, A22 = cos,                 
                A33 = 1
            };
        }

        public static Matrix CreateReflect(Line edge)
        {
            return CreateMove(edge.A) * CreateReflect(edge.AB) * CreateMove(-edge.A);
        }

        public static Matrix CreateIdentity()
        {
            Matrix m = new Matrix();
            m.A11 = 1; m.A22 = 1; m.A33 = 1;
            return m;
        }
    }
}
