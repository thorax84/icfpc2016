using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Fractions;
using Platform.Geometry;
using Platform.Models;

namespace Platform
{
    public class SolutionFormatter
    {
        public string GetSolutionSpecString(SolutionModel model)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(model.Points.Count.ToString());
            foreach (var point in model.Points)
            {
                sb.AppendLine(FormatPoint(point.Src));
            }

            sb.AppendLine(model.Polygons.Count.ToString());
            foreach (var polygon in model.Polygons)
            {
                WritePolygon(sb, polygon);
                sb.AppendLine();
                
            }

            foreach (var point in model.Points)
            {
                sb.AppendLine(FormatPoint(point.Dest));
            }

            return sb.ToString();
        }

        public Int32 GetSolutionSize(string solution)
        {
            var cutString = Regex.Replace(solution, @"[\s]", "");
            return cutString.Length;

        }

        public Int32 GetSolutionSize(SolutionModel solution)
        {
            var str = GetSolutionSpecString(solution);
            return GetSolutionSize(str);
        }

        private void WritePolygon(StringBuilder sb, List<int> polygon)
        {
            sb.Append(polygon.Count);
            sb.Append(" ");
            sb.Append(String.Join(" ", polygon));
        }

        private string FormatPoint(Vector point)
        {
            return $"{FormatFraction(point.X)},{FormatFraction(point.Y)}";
        }

        private string FormatFraction(Fraction a)
        {
            // ��� 0 Fraction ��� ������ "0/0". ��� ���������� ��� ����� ��������
            if (a == 0)
                return "0";
            return a.ToString();
        }
    }
}