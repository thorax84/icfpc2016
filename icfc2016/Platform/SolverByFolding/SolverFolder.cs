﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fractions;
using Platform.ExecutionPlatform;
using Platform.Geometry;
using Platform.Models;

namespace Platform.SolverByFolding
{
    public class SolverFolder : IResolver
    {
        Folder _folder = new Folder();


        public SolverResult SolveProblem(ProblemModel problem, bool keepOptimalTransform = false /*только для отображения*/)
        {
            // переместить силует внутрь единичного листа 
            
            //var boundingRect = GetBounds(problem.Silhouette);

            var optimalTransform = ProblemPositionOptimizer.CalcOptimalTransform(problem);
            problem.Transform(optimalTransform);

            //if(boundingRect.Width > 1 || boundingRect.Height > 1)
            //    throw  new NotSupportedException("поворачивать пока не умею");

            //var shiluetteTransform = Matrix.CreateMove(-boundingRect.Bl);

            //var movedSilhouette = TransformSilhouette(problem.Silhouette, optimalTransform);
            // тут силует уже должен быть внутри листа
            //Debug.Assert(GetBounds(movedSilhouette).IsIn(IcfcRect.Simple));

            
            var allPoints = problem.Silhouette.SelectMany(x => x.Verticles).ToList();
            var allEdges = problem.Silhouette.SelectMany(x => x.Edges);

            var convexHull = GeometryUtils.ComputeConvexHull(allPoints);

            _folder.Reset();

            for (int i = 0; i < 1; i++)
            {
                var prevPoint = convexHull.Last();
                foreach (var point in convexHull)
                {
                    _folder.Fold(new Line(prevPoint, point));
                    prevPoint = point;
                }
            }

            var solution = _folder.GetSolution();

            if(!keepOptimalTransform) //только для отображения
                solution.Transform(optimalTransform.Invert());

            return new SolverResult()
            {
                Solution = solution,
                MovedShiluette = problem.Silhouette,

            };
        }

        private Boolean CalcIsBoundaryEdge(Line edge, List<Vector> allPoints)
        {
            // проверим что у всех точек одинаковый sideOfLine
            var sideOfLine = SideOfLine.OnLine;
            foreach (var point in allPoints)
            {
                var pointSide = edge.GetSideOfLine(point);
                if (sideOfLine != SideOfLine.OnLine && pointSide != sideOfLine)
                    return false;
                sideOfLine = pointSide;
            }

            return true;
        }

        private IcfpPolygon[] TransformSilhouette(IcfpPolygon[] silhouette, Matrix shiluetteTransform)
        {
            return silhouette.Select(x => x.Transform(shiluetteTransform)).ToArray();
        }

        private IcfcRect GetBounds(IcfpPolygon[] silhouette)
        {
            IcfcRect ret = new IcfcRect();
            var forstVerticle = silhouette[0].Verticles[0];
            ret.L = ret.R = forstVerticle.X;
            ret.T = ret.B = forstVerticle.Y;

            foreach (var point in silhouette.SelectMany(x => x.Verticles))
            {
                ret.L = FractionMath.Min(point.X, ret.L);
                ret.R = FractionMath.Max(point.X, ret.R);
                ret.T = FractionMath.Max(point.Y, ret.T);
                ret.B = FractionMath.Min(point.Y, ret.B);
            }
            return ret;
        }

    }

    public class SolverResult
    {
        public SolutionModel Solution;
        public IcfpPolygon[] MovedShiluette;
    }


}