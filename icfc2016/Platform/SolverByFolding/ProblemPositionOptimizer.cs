﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Platform.Geometry;
using Platform.Models;
using Fractions;
using Newtonsoft.Json;

namespace Platform.SolverByFolding
{
    public class ResultModel
    {
        public Fraction Sin, Cos;
        public Double AngleRad;
        public Double AngleGrad;

    }

    public class ProblemPositionOptimizer
    {
        private static ResultModel[] goodAngles = ReadrationalTrigonometry();

        private static ResultModel[] ReadrationalTrigonometry()
        {
            var rationalTrigonometryStream = typeof(ProblemPositionOptimizer).Assembly.GetManifestResourceStream("Platform.rationalTrigonometry.json");
            StreamReader s = new StreamReader(rationalTrigonometryStream);
            var goodAnglesJson = s.ReadToEnd();
            return JsonConvert.DeserializeObject<ResultModel[]>(goodAnglesJson);

        }

        public static Matrix CalcOptimalTransform(ProblemModel problem)
        {

            var allPoints = problem.Silhouette.SelectMany(x => x.Verticles).ToList();            
            var convexHull = GeometryUtils.ComputeConvexHull(allPoints);

            var center = GeometryUtils.GetCenterOfPointsSet(convexHull);

            var origRect = GeometryUtils.GetBoundRect(convexHull);

            var minRectSqr = origRect.GetSquare();
            var bestMx = Matrix.CreateIdentity();

            foreach(var angle in goodAngles)
            {
                var rotMatrix = Matrix.CreateRotation(angle.Sin, angle.Cos);
                var moveToZeroMatrix = Matrix.CreateMove(-center);
                var transAndRotMx = rotMatrix * moveToZeroMatrix;

                var rotatedHull = convexHull.Select(p => transAndRotMx * p).ToList();

                var rect = GeometryUtils.GetBoundRect(rotatedHull);
                var rectSquare = rect.GetSquare();
                if (rectSquare < minRectSqr)
                {
                    minRectSqr = rectSquare;
                    bestMx = Matrix.CreateMove(-rect.Bl) * transAndRotMx;
                }
            }

            return bestMx;
        }
    }
}
