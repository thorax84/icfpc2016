﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Fractions;
using Newtonsoft.Json;
using Platform.Geometry;
using Platform.Models;
using Vector = Platform.Geometry.Vector;
using System.Diagnostics;
using System.IO;
using Platform;
using Platform.SolverByFolding;
using Path = System.Windows.Shapes.Path;

namespace FolderTool
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainVm Vm;
        

        public Folder folder;

        public MainWindow()
        {
            InitializeComponent();
            folder = new Folder();
        }

        private void MainWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            Vm = RestoreVm();
            
            DataContext = Vm;
        }

        private MainVm RestoreVm()
        {
            var saved = Properties.Settings.Default.Vm;

            if (String.IsNullOrWhiteSpace(saved))
                return DefaultVm();

            try
            {
                return JsonConvert.DeserializeObject<MainVm>(saved);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error restoring settings " + ex.Message);
                return DefaultVm();
            }
        }

        private MainVm DefaultVm()
        {
            return new MainVm()
            {
                Folds = new ObservableCollection<LineVm>(),
                TaskId = 1,
            };
        }

        private void Reset_Click(object sender, RoutedEventArgs e)
        {
            folder.Reset();
            DisplaySolution(folder.GetSolution());
        }

        private void Validate_Click(object sender, RoutedEventArgs e)
        {
            var solution = folder.GetSolution();
            //var solution = DemoTasks.demoTask3SolutionModel;
            var testTime = new DateTime(2016, 8, 7, 21, 0, 0, DateTimeKind.Utc);
            try
            {
                var api = new ApiHandler.ApiHandler();

                var respond = api.SubmitProblem(testTime, solution);

                Debug.Assert(respond.ok);

                MessageBox.Show($"VALID :) id={respond.problem_id} size={respond.solution_size}");
            }
            catch(Exception ex)
            {
                MessageBox.Show("INVALID :( " + ex.Message);
            }

            
        }

        private void Load_Click(object sender, RoutedEventArgs e)
        {
            LoadCustomFolder();

            var solution = folder.GetSolution();

            DisplaySolution(solution);
        }

        void DisplaySolution(SolutionModel solution, IcfpPolygon[] additional = null)
        {
            PathGeometry srcGeom, dstGeom;
            SolutionFormatter f = new SolutionFormatter();
            var size = f.GetSolutionSize(solution);
            _solutionSizeLabel.Content = size;

            RenderSolution(solution, out srcGeom, out dstGeom);

            _srcGeomhost.Children.Clear();

            var srcControl = new Path
            {
                Fill = new SolidColorBrush(Color.FromArgb(40, 50, 100, 100)),
                Stroke = Brushes.Black,
                StrokeThickness = 1,
                Stretch = Stretch.Uniform,
                Data = srcGeom,
                //RenderTransform = new ScaleTransform(1,-1)
            };
            _srcGeomhost.Children.Add(srcControl);

            _dstGeomhost.Children.Clear();

            // добавление осей
            {
                var axisPathGeom = new PathGeometry();

                axisPathGeom.Figures.Add(new PathFigure(new Point(-1, 0), new List<PathSegment> { new LineSegment(new Point(1, 0), true) }, false));
                axisPathGeom.Figures.Add(new PathFigure(new Point(0, -1), new List<PathSegment> { new LineSegment(new Point(0, 1), true) }, false));

                var axis = new Path
                {
                    Stroke = Brushes.Black,
                    StrokeThickness = 0.01,
                    Stretch = Stretch.None,

                    Data = axisPathGeom
                };
                _dstGeomhost.Children.Add(axis);
            }


            if (additional != null)
            {
                var additionalPathGeom = new PathGeometry();

                var figs = RenderPoligons(additional);
                foreach (var pathFigure in figs)
                {
                    additionalPathGeom.Figures.Add(pathFigure);
                }

                var additionalPath = new Path
                {
                    Fill = new SolidColorBrush(Color.FromArgb(100, 100, 100, 100)),
                    Stroke = Brushes.Black,
                    StrokeThickness = 0.005,
                    Stretch = Stretch.None,

                    Data = additionalPathGeom
                };
                _dstGeomhost.Children.Add(additionalPath);
            }

            

            var dstControl = new Path
            {
                Fill = new SolidColorBrush(Color.FromArgb(40, 250, 50, 50)),
                Stroke = Brushes.Black,
                StrokeThickness = 0.002,
                Stretch = Stretch.None,
                
                Data = dstGeom
            };
            _dstGeomhost.Children.Add(dstControl);




        }

        private void Random_Click(object sender, RoutedEventArgs e)
        {
            folder.Reset();
            folder.FoldRandom(4);
            DisplaySolution(folder.GetSolution());
        }

        private void RenderSolution(SolutionModel solution, out PathGeometry srcGeoms, out PathGeometry dstGeoms)
        {
            srcGeoms = new PathGeometry();
            dstGeoms = new PathGeometry();

            foreach (var poly in solution.Polygons)
            {
                var srcFig = new PathFigure() { IsClosed = true };
                var dstFig = new PathFigure() { IsClosed = true };
                srcGeoms.Figures.Add(srcFig);
                dstGeoms.Figures.Add(dstFig);

                var isFirst = true;
                foreach (var pointIndex in poly)
                {
                    var point = solution.Points[pointIndex];

                    var srcWpfPoint = new Point(point.Src.X.ToDouble(), point.Src.Y.ToDouble());
                    var dstWpfPoint = new Point(point.Dest.X.ToDouble(), point.Dest.Y.ToDouble());

                    if (isFirst)
                    {
                        srcFig.StartPoint = srcWpfPoint;
                        dstFig.StartPoint = dstWpfPoint;
                        isFirst = false;
                    }
                    else
                    {
                        srcFig.Segments.Add(new LineSegment(srcWpfPoint, true));
                        dstFig.Segments.Add(new LineSegment(dstWpfPoint, true));
                    }
                }
            }
        }

        private List<PathFigure> RenderPoligons(IEnumerable<IcfpPolygon> poligons)
        {
            List<PathFigure> ret = new List<PathFigure>();
            foreach (var poly in poligons)
            {
                var srcFig = new PathFigure { IsClosed = true };
                ret.Add(srcFig);

                var isFirst = true;
                foreach (var vertex in poly.Verticles)
                {
                    var srcWpfPoint = new Point(vertex.X.ToDouble(), vertex.Y.ToDouble());

                    if (isFirst)
                    {
                        srcFig.StartPoint = srcWpfPoint;
                        isFirst = false;
                    }
                    else
                    {
                        srcFig.Segments.Add(new LineSegment(srcWpfPoint, true));
                    }
                }
            }
            return ret;
        }

        private void LoadCustomFolder()
        {
            folder.Reset();

            foreach (var fold in Vm.Folds)
            {
                folder.Fold(
                new Platform.Geometry.Line(
                    new Platform.Geometry.Vector(fold.FromX, fold.FromY),
                    new Platform.Geometry.Vector(fold.ToX, fold.ToY)));
            }

            folder.CalcSrc();            
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            Vm.Folds.Add(new LineVm { FromX = 0, FromY = 1 });
        }

        private void Remove_Click(object sender, RoutedEventArgs e)
        {
            var sel = _foldsListbox.SelectedItems.Cast<LineVm>().ToList();
            
            foreach (var o in sel)
            {
                Vm.Folds.Remove((LineVm) o);
            }
        }

        private void MainWindow_OnClosed(object sender, EventArgs e)
        {
            var vmSave = JsonConvert.SerializeObject(Vm);
            Properties.Settings.Default.Vm = vmSave;
            
            Properties.Settings.Default.Save();
        }

        private void LoadAndSolveTask(object sender, RoutedEventArgs e)
        {
            try
            {
                var taskId = Vm.TaskId;
                var resources = typeof (App).Assembly.GetManifestResourceNames();

                var resourceName = resources.First(x => x.Contains($"problem_{taskId}_"));

                var resStream = typeof (App).Assembly.GetManifestResourceStream(resourceName);
                StreamReader r = new StreamReader(resStream);
                var problemStr = r.ReadToEnd();
                var problem = ProblemParser.ParseProblem(problemStr);
                
                SolverFolder s = new SolverFolder();
                var result = s.SolveProblem(problem, true);

                DisplaySolution(result.Solution, result.MovedShiluette);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
    }

    class FractionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var fr = (Fraction)value;
            if (fr.IsZero) return "0";
            return fr.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var frStr = (String)value;
            return Fraction.FromString(frStr);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }

    public class LineVm : INotifyPropertyChanged
    {
        private Fraction _fromX;
        private Fraction _fromY;

        public Fraction FromX
        {
            get { return _fromX; }
            set
            {
                _fromX = value;
                OnPropertyChanged(nameof(FromX));
            }
        }

        public Fraction FromY
        {
            get { return _fromY; }
            set
            {
                _fromY = value;
                OnPropertyChanged(nameof(FromY));
            }
        }


        private Fraction _toX;
        private Fraction _toY;

        public Fraction ToX
        {
            get { return _toX; }
            set
            {
                _toX = value;
                OnPropertyChanged(nameof(ToX));
            }
        }

        public Fraction ToY
        {
            get { return _toY; }
            set
            {
                _toY = value;
                OnPropertyChanged(nameof(ToY));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

}
