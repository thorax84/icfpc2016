using System;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace FolderTool
{
    public class MainVm : INotifyPropertyChanged
    {
        private ObservableCollection<LineVm> _folds;
        private int _taskId;

        public ObservableCollection<LineVm> Folds   
        {
            get { return _folds; }
            set
            {
                _folds = value;
                OnPropertyChanged(nameof(Folds));
            }
        }

        public Int32 TaskId
        {
            get { return _taskId; }
            set
            {
                _taskId = value;
                OnPropertyChanged(nameof(TaskId));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}