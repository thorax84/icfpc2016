﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Platform.Geometry;
using Fractions;
using Newtonsoft.Json;
using Platform.SolverByFolding;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            
        }

        private static void CalcGoodAngles()
        {
            List<Int32[]> rawResults = new List<int[]>();

            var max = 200;

            for (int a = 0; a < max; a++)
            {
                for (double b = 0; b < max; b++)
                {
                    for (int c = 0; c < max; c++)
                    {
                        for (double d = 0; d < max; d++)
                        {
                            if (a == b || c == d)
                                continue;
                            var ok = (a / b) * (a / b) + (c / d) * (c / d) == 1;
                            if (ok)
                            {
                                rawResults.Add(new[] { a, (int)b, c, (int)d });
                            }
                        }
                    }
                }
            }

            var results = rawResults
                .Select(GetResultModel)
                .Distinct(new ResultComparer())
                .OrderBy(x => x.AngleRad);



            var resultTerxt = JsonConvert.SerializeObject(results, Formatting.Indented);
            File.WriteAllText("rationalTrigonometry.json", resultTerxt);
        }

        private static ResultModel GetResultModel(int[] x)
        {
            var a = Math.Asin(x[0]/(double) x[1]);
            return new ResultModel()
            {
                Sin = new Fraction(x[0], x[1]),
                Cos = new Fraction(x[2], x[3]),
                AngleRad = a,
                AngleGrad = 180 / Math.PI * a
            };
        }
    }

    internal class ResultComparer : IEqualityComparer<ResultModel>
    {
        public bool Equals(ResultModel x, ResultModel y)
        {
            return x.AngleRad == y.AngleRad;
        }

        public int GetHashCode(ResultModel obj)
        {
            return obj.AngleRad.GetHashCode();
        }
    }

    class ResultModel
    {
        public Fraction Sin, Cos;
        public Double AngleRad;
        public Double AngleGrad;

    }
}
